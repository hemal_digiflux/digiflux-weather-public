Customizable Weather app by Digiflux.

Contact: info@digiflux.io

https://www.telerik.com/blogs/dockerizing-react-applications-for-continuous-integration


Steps:
- Extract source file
- Goto terminal
- Command:- “cd weatherapp”
- Command:- “npm install”
- Command:- “docker-compose build web” - Command:- “docker-compose up web”
- Go to chrome and type “http://localhost/" - Search for city and check the weather
Info:- App is node based React JS app and also Firebase enabled for hosting. App is also Dockerized so it could be maintained easily.
We have tried implementing the features in our way which was mentioned in the chat.
Note- “Suggestions are partially working as there is some ApiKeyActivationError issue again and again from google, as we used Places API for Suggestions of cities.”That will be also delivered shortly”